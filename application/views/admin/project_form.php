<?php defined('SYSPATH') or die('No direct script access.');
/**
 * project_form.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 2/12/15
 * Time: 4:32 AM
 * Copyright 2015
 */
?>
<div class="container">
    <h1>Новый проект</h1>
    <form action="/admin/projadd" method="post">
        <div class="form-group <?= (isset($errors['project_name']))? 'has-error': ''?>">
            <label for="project_name">Название проекта <span style="color: #ff0000;">*</span> </label>
            <input type="text" name="project_name" id="project_name" class="form-control" value="<?= isset($post['project_name'])? $post['project_name'] : '' ?>" maxlength="100"/>
        </div>
        <div class="form-group <?= (isset($errors['pages'][0]['page_addr']))? 'has-error': ''?>">
            <p class="form-control-static">Страница вариант 1</p>
            <label for="page_addr[]">Адрес <span style="color: #ff0000;">*</span></label>
            <input type="text" class="form-control" name="page_addr[]" id="page_addr[]" maxlength="100" value="<?= isset($post['page_addr'][0])? $post['page_addr'][0]: '' ?>"/>
            <label for="page_descr[]">Примечание</label>
            <input type="text" class="form-control" name="page_descr[]" id="page_descr[]" maxlength="100"/>
        </div>
        <div class="form-group <?= (isset($errors['pages'][1]['page_addr']))? 'has-error': ''?>">
            <p class="form-control-static">Страница вариант 2</p>
            <label for="page_addr[]">Адрес <span style="color: #ff0000;">*</span> </label>
            <input type="text" class="form-control" name="page_addr[]" id="page_addr[]" maxlength="100" value="<?= isset($post['page_addr'][1])? $post['page_addr'][1]: '' ?>"/>
            <label for="page_descr[]">Примечание</label>
            <input type="text" class="form-control" name="page_descr[]" id="page_descr[]" maxlength="100"/>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Сохранить"/>
            <a href="/admin/projects" class="btn btn-warning">Отмена</a>
        </div>
    </form>
    <p><span style="color: #ff0000;">*</span> - обязательные поля</p>
</div>