<?php defined('SYSPATH') or die('No direct script access.');
/**
 * codeTpl.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 11.02.15
 * Time: 18:11
 * Copyright 2015
 */

//todo темплейт кода для вставки в страницы

?>
<!-- AB Test example. Place this code at header of first (main) page -->
<script type="text/javascript">

    (function(){
        var this_page_id = '<?= $project_id ?>';
        var project_id = '<?= $page_id ?>';
        $.cookie('project_id',project_id);

        function getUrlVars(){
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        if($.cookie('visitor_id')){

            var vars = getUrlVars();
            var visitor_id = $.cookie('visitor_id');

            if (visitor_id) {
                $.ajax({
                    url: '<?= $get_pageid_url ?>',
                    type: 'POST',
                    data: {visitor_id: visitor_id, project_id: project_id},
                    dataType: 'json',
                    success: function (data) {
                        if (this_page_id != data.page_id) {
                            window.location = data.page_addr;
                        }
                    }
                });
            }
        }else{
            $.ajax({
                url:'<?= $new_visitor_url ?>',
                type:'POST',
                data:{project_id:project_id},
                dataType:'json',
                success: function (data) {
                    $.cookie('visitor_id',data.visitor_id,{expires:data.expires});
                    $.cookie('page_id',data.page_id);
                    if(this_page_id != data.page_id){
                        window.location = data.page_addr;
                    }
                }
            });
        }

    })();

</script>
<!-- END AB Test Example -->