<?php defined('SYSPATH') or die('No direct script access.');
/**
 * loginform.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 11.02.15
 * Time: 18:32
 * Copyright 2015
 */
?>
<div class="container" style="margin-top:30px">
    <div class="col-md-12">
        <div class="modal-dialog" style="margin-bottom:0">
            <div class="modal-content">
                <div class="panel-heading">
                    <h3 class="panel-title">Вход</h3>
                </div>
                <div class="panel-body">
                    <form role="form" method="post" action="/admin/login">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Имя пользователя" name="username" type="username" autofocus="">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Пароль" name="password" type="password" value="">
                            </div>
                            <input type="submit" class="btn btn-sm btn-success" value="Войти"/>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>