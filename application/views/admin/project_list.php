<?php defined('SYSPATH') or die('No direct script access.');
/**
 * project_list.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 11.02.15
 * Time: 18:05
 * Copyright 2015
 */

//todo список проектов пользователя
?>
<div class="container">
    <div class="row col-md-12 custyle">
        <table class="table table-striped custab">
            <thead>
            <a href="/admin/projadd" class="btn btn-primary btn-xs pull-right"><b>+</b> Добавить новый проект</a>
            <tr>
                <th>ID</th>
                <th>Проект</th>
                <th class="text-center">Действия</th>
            </tr>
            </thead>
            <tbody>
            <?php
            /** @var Model_Project $project */
            foreach($projects as $project){
                ?>
                <tr>
                    <td><?= $project->id ?></td>
                    <td><?= $project->project_name ?></td>
                    <td class="text-center">
                        <a class='btn btn-info btn-xs' href="/admin/projview?id=<?= $project->id ?>">
                            <span class="glyphicon glyphicon-eye-open"></span> Смотреть
                        </a>
                        <a href="/admin/projdel?id=<?= $project->id ?>" class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-remove"></span> Удалить
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>