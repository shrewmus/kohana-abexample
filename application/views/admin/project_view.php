<?php defined('SYSPATH') or die('No direct script access.');
/**
 * project.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 11.02.15
 * Time: 18:06
 * Copyright 2015
 */

/** @var $project Model_Project */

//todo страница проекта
?>
<div class="container">
    <div class="row">
        <h1>Данные по проекту</h1>
        <div class="col-md-10">
            <div class="col-md-4"> Название</div>
            <div class="col-md-6"><strong><?= $project->project_name ?></strong></div>
        </div>
        <div class="col-md-12">
            <div class="col-md-2"> Код для вставки</div>
            <div class="col-md-10">
                <pre style="height: 400px; overflow-y: scroll;"><code><?= htmlentities($code) ?></code></pre>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-2">Код счетчика для вставки</div>
            <div>
                <pre style="height: 100px; overflow-y: scroll;"><code><?= htmlentities($counter) ?></code></pre>
            </div>
        </div>
    </div>
    <div class="row">
        <h1>Статистика по страницам</h1>
            <?php
            $pages = $project->pages->find_all();
            /** @var Model_Page $page */
            foreach($pages as $page){ ?>
            <div class="col-md-12">
                <div class="col-md-10">
                    <div class="col-md-2">Примечание</div>
                    <div class="col-md-8"><?= $page->page_descr ?></div>
                </div>
                <div class="col-md-10">
                    <div class="col-md-2">Адрес</div>
                    <div class="col-md-8"><?= $page->page_addr ?></div>
                </div>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Количество просомотров</th>
                            <th>Количество кликов по кнопке</th>
                            <th>Количестов заполнений формы</th>
                            <th>Конверсия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?= $page->vis_cnt ?></td>
                            <td><?= $page->click_cnt ?></td>
                            <td><?= $page->form_edit_openpage ?></td>
                            <td><?php
                                if ($page->form_edit_openpage > 0) {
                                    echo round($page->form_edit_openpage
                                            / $page->vis_cnt * 100,2)
                                        . " %";
                                } else {
                                    echo " - ";
                                }
                                ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
<!--                echo $page->page_descr."<br/>";-->
            <?php } ?>
            </div>
    </div>
</div>