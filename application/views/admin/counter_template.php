<?php
/**
 * counter_template.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 13.02.15
 * Time: 16:37
 * Copyright 2015
 */
?>
<!-- AB Counter -->
<script type="text/javascript">
    (function(){
        var visitor_id = $.cookie('visitor_id');
        var page_id = $.cookie('page_id');
        $.ajax({
            url: '<?= $vis_counter_url ?>',
            type: 'POST',
            data:{visitor_id:visitor_id,page_id: page_id }
        });
    })();
</script>
<!-- END AB Counter -->