<?php
/**
 * added_counter.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 13.02.15
 * Time: 17:41
 * Copyright 2015
 */
?>
<script type="text/javascript">
    (function(){
        var visitor_id = $.cookie('visitor_id');
        var page_id = $.cookie('page_id');
        $.ajax({
            url: '/default/viscounter',
            type: 'POST',
            data:{visitor_id:visitor_id,page_id: page_id }
        });
    })();
</script>