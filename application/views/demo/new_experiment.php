<?php defined('SYSPATH') or die('No direct script access.');
/**
 * new_experiment.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 13.02.15
 * Time: 12:47
 * Copyright 2015
 */
?>
<div class="container">
    <h1>Эксперимент: Новая страница (Страница 2)</h1>

    <h2>Красная кнопка</h2>
    <a href="/default/abtarget" class="btn btn-danger">Кликните сюда для
        исследования</a>
</div>