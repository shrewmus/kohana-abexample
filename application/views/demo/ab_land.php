<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ab_land.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 13.02.15
 * Time: 12:57
 * Copyright 2015
 */
?>
<div class="container">
    <form action="<?= $ab_form_action ?>" method="post">
        <div class="form-group">
            <label>Ваше впечатление?</label>
            <input type="text" name="confirm" value=""/>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Отправить"/>
            <a href="/default/experiment/1" class="btn btn-warning">Отмена</a>
        </div>
    </form>
</div>