<?php defined('SYSPATH') or die('No direct script access.');
/**
 * experiment.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12.02.15
 * Time: 11:38
 * Copyright 2015
 */
?>
<div class="container">
    <h1>Эксперимент: Исходная страница (Страница 1)</h1>

    <h2>Синяя кнопка</h2>
    <a href="/default/abtarget" class="btn btn-primary">Кликните сюда для
        исследования</a>
</div>