<?php defined('SYSPATH') or die('No direct script access.');
/**
 * admin_layout.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 11.02.15
 * Time: 15:20
 * Copyright 2015
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title><?= $title ?></title>
    <?php
    foreach($cssFiles as $cssFile){
        echo HTML::style($cssFile['lnk'],$cssFile['attr']);
    }
    foreach ($jsFiles['head'] as $jsFile) {
        echo HTML::script($jsFile);
    }

    foreach($headScript as $script){
        echo $script;
    }

    ?>
</head>
<body>
    <div class="navbar navbar-inverse navbar-static-top">

            <div class="container">
<!--                <div class="navbar-header">-->
<!--                    <a class="navbar-brand" href="/">CMS</a>-->
<!--                </div>-->
                <ul class="nav navbar-nav">
                    <li class="divider-vertical"></li>
                    <?php if(Auth::instance()->logged_in()){ ?>
                    <li><a href="/admin/projects"><span class="glyphicon glyphicon-home"></span> Проекты</a></li>
                    <?php }else{ ?>
<!--                        <li><a href="/"><span class="glyphicon glyphicon-home"></span> </a></li>-->
                    <?php } ?>
                </ul>

                <?php if (Auth::instance()->logged_in()) { ?>
                    <p class="navbar-text navbar-right"><a href="/admin/logout" class="navbar-link"><span class="glyphicon glyphicon-log-out"></span> Выход</a></p>
                <?php } ?>
                <p class="navbar-text navbar-right"><span class="glyphicon glyphicon-user"></span> Пользователь <span class="navbar-link">
                        <?= (Auth::instance()->logged_in()) ? Auth::instance()->get_user()->username : 'guest' ?></span>
                </p>


            </div>

    </div>
    <?= $content ?>

    <?php
    foreach($jsFiles['end'] as $jsFile){
        echo HTML::script($jsFile);
    }
    ?>
</body>
</html>