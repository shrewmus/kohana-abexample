<?php defined('SYSPATH') or die('No direct script access.');
/**
 * validation.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12.02.15
 * Time: 11:17
 * Copyright 2015
 */

return [
        'not_empty' => '<b>:field</b> - обязательное поле',
        'max_length'=>'Длина значения в <b>:field</b> должна быть не больше <b>:param2</b> знаков',
];