<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Page.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 11.02.15
 * Time: 18:25
 * Copyright 2015
 *
 * @property int $id
 * @property int $project_id
 * @property string $page_addr
 * @property string $page_descr
 * @property int $vis_cnt
 * @property int $click_cnt
 * @property int $form_edit_openpage
 *
 * @property Model_Project $project
 * @property Model_Visitor[] $visitors
 */
class Model_Page extends ORM{

    public function labels(){
        return [
            'page_addr' => 'Адрес страницы'
        ];
    }

    public function rules(){
        return[
            'page_addr' => [
                ['not_empty'],
                ['max_length',[':value',100]]
            ]
        ];
    }

    protected $_belongs_to = [
        'project' => []
    ];

    protected $_has_many = [
        'visitors'=>[
            'model' =>'Visitor',
            'through' => 'visitors_pages'
        ]
    ];

}