<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Visitor_Page.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12.02.15
 * Time: 17:29
 * Copyright 2015
 *
 * @property int $page_id
 * @property int $visitor_id
 * @property datetime $first_visit
 *
 * @property Model_Page $page
 * @property Model_Visitor $visitor
 */
class Model_VisitorPage extends ORM{

    protected $_table_name = 'visitors_pages';

    public function save(Validation $validation = null) {
        $this->first_visit = date("Y-m-d H:i:s",(new DateTime('now'))->getTimestamp());
        return parent::save($validation);
    }

    protected $_belongs_to = [
        'page' =>[],
        'visitor' => []
    ];

}