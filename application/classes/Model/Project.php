<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Project.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 11.02.15
 * Time: 18:24
 * Copyright 2015
 *
 * @property int $id
 * @property int $user_id
 * @property string $project_name
 * @property int $cur_page_id
 * @property Model_Page[] $pages
 */
class Model_Project extends ORM{



    public function rules(){


        return [
            'project_name' => [
                ['not_empty'],
                ['max_length',[':value',100]]
            ]
        ];
    }

    public function labels(){
        return [
            'project_name' => 'Название проекта'
        ];
    }

    protected $_has_many = [
        'pages'=>[
            'model' => 'Page',
        ]
    ];

}