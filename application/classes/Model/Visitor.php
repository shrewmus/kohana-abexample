<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Visitor.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 12.02.15
 * Time: 12:50
 * Copyright 2015
 *
 * @property int $id
 */
class Model_Visitor extends ORM{

    protected $_has_many = [
        'pages'=>[
            'model' => 'Page',
            'throug'=>'visitors_pages'
        ]
    ];

}