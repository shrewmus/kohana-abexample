<?php defined('SYSPATH') or die('No direct script access.');
//todo transform to assets module

class Controller_Base extends Controller_Template{

    //варианты подключения js скриптов
    const JS_HEAD = 'head';
    const JS_END = 'end';

    /** @var string | View */
    public $template = 'layout/admin_layout';
    public $layoutVars;

    public function before() {
        parent::before();
        //Дефолнтые значения для переменных шаблона
        $this->layoutVars = [
            'title' =>'',
            'title'=>'',
            'cssFiles'=> [],
            'jsFiles'=>[ self::JS_HEAD =>[], self::JS_END =>[]],
            'headScript' =>[],
            'content'=>''
        ];
    }

    /**
     * Добавление js файлов (ссылка на assets или внешний)
     * @param $jsFile
     * @param string $place
     */
    protected function addJSFile($jsFile,$place = self::JS_HEAD ){
        if (is_array($jsFile)) {
            foreach ($jsFile as $file) {
                $this->addJSFile($file, $place);
            }
        }else{
            $this->layoutVars['jsFiles'][$place] = $jsFile;
        }
    }

    /**
     * Добавление css (ссылка на assets или внешний)
     * @param $file
     * @param string $media
     */
    protected function addCssFile($file,$media = 'screen'){
        if(is_array($file)){
            foreach($file as $cssElem){
                if(is_array($cssElem)){
                    $this->addCssFile($cssElem['file'], isset($cssElem['media']) ? $cssElem['media'] : $media);
                }else{
                    $this->addCssFile($cssElem);
                }
            }
        }else{
            $this->layoutVars['cssFiles'][] = ['lnk' => $file, 'attr' => ['media' => $media]];
        }
    }

    /**
     * Вставка js скрипта
     * без файла, напрямую код
     * @param $data
     */
    protected function addHeadScript($data){
        $this->layoutVars['headScript'][] = $data;
        //todo style with inline code + place js to end page
    }

    /**
     * Установка основного контента
     * @param $content
     * @param bool $isAdd флаг добавлять к уже существующему или перезаписывать (по-умолчанию)
     */
    protected function setContent($content, $isAdd = false){
        if($isAdd){
            $content = $this->layoutVars['content']." ".$content;
        }
        $this->layoutVars['content'] = $content;
    }

    /**
     * Установка переменных контроллера в переменные лейаута (вида)
     */
    public function after() {
        $this->template->set($this->layoutVars);
        parent::after();
    }


}