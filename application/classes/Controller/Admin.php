<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Пример админки для настройки аб тестирования
 * Admin.php
 * Author: shrewmus (shrewmus@yandex.ru, contact@shrewmus.name)
 * Date: 11.02.15
 * Time: 18:27
 * Copyright 2015
 */

class Controller_Admin extends Controller_Base{

    /**
     * @inheritdoc
     */
    public function before() {
        parent::before();

        //проверка авторизацци
        if($this->request->action() != 'login'){
            if(! Auth::instance()->logged_in()){
                $this->redirect('/admin/login');
            }
        }


        //Подключение скриптов и стилей
        $this->layoutVars['cssFiles'][] = ['lnk' => '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css','attr'=>[]];
        $this->layoutVars['cssFiles'][] = ['lnk' => '/assets/css/appstyle.css?tm='.(new DateTime('now'))->getTimestamp(),'attr'=>[]];
        $this->layoutVars['jsFiles'][Controller_Base::JS_HEAD][] = 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js';
        $this->layoutVars['jsFiles'][Controller_Base::JS_HEAD][] = 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.map';
        $this->layoutVars['jsFiles'][Controller_Base::JS_HEAD][] = '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js';
    }

    /**
     * Главная страница в админке - список проектов
     */
    public function action_index(){
        $this->action_projects();
    }

    /**
     * Добавление проекта
     */
    public function action_projadd(){
        $post = $this->request->post();
        //todo сохранение в 2 таблицы без транзакций
        if(empty($post)){//если данных нет - вывод пустой формы
            $this->setContent(View::factory('admin/project_form',['errors'=>[]]));
        }else{//иначе попытка сохранить
            //новый проект + заполнение данными
            $project = new Model_Project();
            $project->user_id = Auth::instance()->get_user()->id;
            $project->project_name = $post['project_name'];
            $projValidation = Validation::factory($post);
            //попытка сохранения проекта
            try{
                $project->check();
            }catch (ORM_Validation_Exception $ex){
                $errors = $ex->errors('validation');
                $this->setContent(View::factory('admin/project_form',['errors'=>$errors,'post'=>$post]));
                return 1;
            }

            $project->save();

            //две страницы для проекта
            foreach($post['page_addr'] as $idx=>$page_addr){
                //новая страница + заполенение полей
                $page = new Model_Page();
                $page->project_id = $project->id;
                $page->page_addr = $page_addr;
                $page->page_descr = $post['page_descr'][$idx];
                try{//попытка сохранения страницы
                    $page->check();
                }catch (ORM_Validation_Exception $ex){//если были ошибки валидации - снова показывается форма к полям добавляется красная рамка
                    $errors = $ex->errors();
                    $this->setContent(View::factory('admin/project_form',['errors'=>['pages'=>[$idx=>$errors]],'post'=>$post]));
                    return 1;
                }
                $page->save();
                if($idx == 0){//текущая страница для нового проекта берется первая
                    $project->cur_page_id = $page->id;
                    $project->save();
                }
            }
            $this->redirect('/admin/projects');
        }
    }

    /**
     * Удаление проекта
     * Страницы, просмотры и т.п. удаляются каскадно по зависимостям через foreign keys
     * @throws Kohana_Exception
     */
    public function action_projdel(){
        $id = $this->request->query('id');
        if($id){
            //todo удаляется без подтверждения
            ORM::factory('Project',$id)->delete();
        }
        $this->redirect('/admin/projects');
    }

    /**
     * Вывод страницы проекта
     */
    public function action_projview(){
        $id = $this->request->query('id');
        /** @var Model_Project $project */
        $project = ORM::factory('Project',$id);//сам проект
        $pages = $project->pages->find_all();//его страницы

        //сгенерированый код для первой (основной) страницы
        $pageCode = View::factory('admin/code_template',[
           'project_id' => $id,
           'page_id' =>$pages[0]->id,
           'get_pageid_url' => '/default/getpageid',
           'new_visitor_url' => '/default/newvisitor'
        ]);

        //сгенерированный код счетчика для обеих страниц
        $pageCounter = View::factory('admin/counter_template',[
            'vis_counter_url' => '/default/viscounter'
        ]);

        $this->setContent(View::factory('admin/project_view',[
            'project'=>$project,
            'code' => $pageCode,
            'counter' => $pageCounter
        ]));
    }

    /**
     * Вывод списка проектов по пользователю
     * @throws Kohana_Exception
     */
    public function action_projects(){
        /** @var Model_User $user */
        $user = Auth::instance()->get_user();

        $projects = new Model_Project();
        $projects->where('user_id','=',$user->id);
        $projects = $projects->find_all();

        $this->setContent(View::factory('admin/project_list',['projects'=>$projects]));
    }


    /**
     * Вывод формы логина, или выполнение авторизации
     */
    public function action_login(){
        $post = $this->request->post();
        if(empty ($post)){
            $this->setContent(View::factory('admin/loginform'));
        }else{
            Auth::instance()->login($post['username'],$post['password']);
            $this->redirect('/admin/projects');
        }
    }

    /**
     * Разлогирование
     */
    public function action_logout(){
        Auth::instance()->logout(true);
        $this->redirect('/admin/login');
    }

    /**
     * Регистрация
     * для теста - ручками меняем private на public и обратно
     * @throws Kohana_Exception
     */
    private function action_register(){
        $model = ORM::factory('User');
        $model->values([
            'username' => 'admin',
            'password' => 'admin',
            'password_confirm' => 'admin',
            'email' => 'shrewmus@yandex.ru'
        ]);
        $model->save();
        $model->add('roles',ORM::factory('Role')->where('name','=','login')->find());
        $model->add('roles',ORM::factory('Role')->where('name','=','admin')->find());
    }

}